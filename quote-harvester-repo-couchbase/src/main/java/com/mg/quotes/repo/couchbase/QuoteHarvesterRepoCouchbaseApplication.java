package com.mg.quotes.repo.couchbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.couchbase.repository.config.EnableCouchbaseRepositories;

import com.mg.quotes.repo.couchbase.repos.RepoRoot;

@SpringBootApplication
@EnableCouchbaseRepositories(basePackageClasses = { RepoRoot.class})
public class QuoteHarvesterRepoCouchbaseApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuoteHarvesterRepoCouchbaseApplication.class, args);
    }

}
