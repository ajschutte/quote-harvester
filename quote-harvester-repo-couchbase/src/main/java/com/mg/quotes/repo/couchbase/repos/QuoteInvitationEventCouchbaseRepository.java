package com.mg.quotes.repo.couchbase.repos;

import org.springframework.data.couchbase.core.query.N1qlPrimaryIndexed;
import org.springframework.data.couchbase.core.query.ViewIndexed;
import org.springframework.data.couchbase.repository.CouchbasePagingAndSortingRepository;

import com.mg.quotes.models.QuoteInvitationEvent;

@N1qlPrimaryIndexed
@ViewIndexed(designDoc = "quoteInvitationEvent")
public interface QuoteInvitationEventCouchbaseRepository extends CouchbasePagingAndSortingRepository<QuoteInvitationEvent, String> {

}
