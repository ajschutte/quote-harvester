package com.mg.quotes.repo.couchbase.repos;

import java.util.Optional;
import java.util.UUID;

import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.mg.quotes.models.EnterpriseIdentifier;
import com.mg.quotes.models.QuoteInvitationEvent;
import com.mg.quotes.models.TransportDescriptor;

/**
 * Integration tests for {@link QuoteInvitationEventCouchbaseRepository}..
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, properties = {
        "management.server.port=-1"
})
public class QuoteInvitationEventCouchbaseRepositoryIT extends BaseRepositoryIT {

    private static final Logger logger = LoggerFactory.getLogger(QuoteInvitationEventCouchbaseRepositoryIT.class);

    @Autowired
    private QuoteInvitationEventCouchbaseRepository qiRepo;

    @Before
    public void init() {
        super.init();
    }

    @Test
    public void storeAndUpdateBasicQuoteInvitationEvent_ok() throws RuntimeException {

        logger.info("Invoking storeAndUpdateBasicQuoteInvitationEvent_ok()..");

        QuoteInvitationEvent qi = createTestQuoteInvitationEvent();

        String created = convertObjectToFormattedJson(qi);

        logger.info("Invoking storeAndUpdateBasicQuoteInvitationEvent_ok(), Created JSON=\n{}", created);

        QuoteInvitationEvent saved = qiRepo.save(qi);

        logger.info("Saved QuoteInvitationEvent={}", saved);

        Assert.assertNotNull(saved);

        Optional<QuoteInvitationEvent> found = qiRepo.findById(qi.getInvitationId());

        Assert.assertTrue(found.isPresent());

        String queried = convertObjectToFormattedJson(found.get());

        logger.info("Invoked storeAndUpdateBasicQuoteInvitationEvent_ok(), Queried JSON=\n{}", queried);

    }

    private QuoteInvitationEvent createTestQuoteInvitationEvent() {

        QuoteInvitationEvent.QuoteInvitationEventBuilder builder = new QuoteInvitationEvent.QuoteInvitationEventBuilder();
        builder
                .isActive(true)
                .withCreated(DateTime.now(), TEST_USER_ID)
                .withUpdated(DateTime.now(), TEST_USER_ID)
                .withEventState(QuoteInvitationEvent.QuoteInvitationEventState.CREATED)
                .withExpiresWhen(DateTime.now())
                .withInvitationId(UUID.randomUUID().toString())
                .withTransactionId(UUID.randomUUID().toString())
                .withOccurredWhen(DateTime.now())
                .withOwningEnterprise(createTestEnterpriseIdentifier())
                .withTransport(createTestTransportDescriptor());

        return builder.build();


    }

    private EnterpriseIdentifier createTestEnterpriseIdentifier() {

        return new EnterpriseIdentifier()
                .withEnterpriseId(TEST_ENTERPRISE_ID)
                .withHierarchyId(TEST_ENTERPRISE_HID)
                .withCustomerId(TEST_CUSTOMER_ID)
                .withOwningEnterpriseId(TEST_CUSTOMER_ID)
                .withApplicationId(TEST_APP_ID);

    }

    private TransportDescriptor createTestTransportDescriptor() {

        return new TransportDescriptor().withTransportId(UUID.randomUUID().toString());

    }

}
