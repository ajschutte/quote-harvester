package com.mg.quotes.ui;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteHarvesterUiApplication {

	public static void main(String[] args) {
		SpringApplication.run(QuoteHarvesterUiApplication.class, args);
	}

}
