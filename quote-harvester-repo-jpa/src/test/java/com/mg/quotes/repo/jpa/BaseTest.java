package com.mg.quotes.repo.jpa;

import static org.junit.Assert.assertNotNull;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.joda.JodaModule;

/**
 * Common base class for all tests..
 */
public abstract class BaseTest {

    protected static final String TEST_USER_ID = "user1234";

    private ObjectMapper mapper;

    public void init() {

        mapper = new ObjectMapper();
        mapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        mapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        mapper.enable(SerializationFeature.WRITE_ENUMS_USING_TO_STRING);
        mapper.enable(DeserializationFeature.READ_ENUMS_USING_TO_STRING);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        mapper.registerModule(new JodaModule());

    }

    protected ObjectMapper getMapper() {
        return mapper;
    }

    protected <T> T parseFromJsonResourceToInstance(
            String resourcePath,
            Class<T> valueType) {

        try {
            return mapper.readValue(loadJsonFromFile(resourcePath), valueType);
        }
        catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

    }

    protected String convertObjectToFormattedJson(Object obj) {

        if (obj instanceof String) {
            return getJsonRepresentationForString((String)obj);
        }
        else {
            return getJsonRepresentationForResource(obj);
        }

    }

    protected String getJsonRepresentationForResource(Object resource) {

        try {
            return getMapper()
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(resource);
        }
        catch (JsonProcessingException e) {
            throw new IllegalArgumentException(e);
        }

    }

    protected String getJsonRepresentationForString(String raw) {

        try {
            Object json = getMapper().readValue(raw, Object.class);
            return getMapper()
                    .writerWithDefaultPrettyPrinter()
                    .writeValueAsString(json);
        }
        catch (Exception e) {
            throw new IllegalArgumentException(e);
        }

    }

    private static String loadJsonFromFile(String file) throws Exception {

        Path path = Paths.get(BaseTest.class
                                      .getResource(file)
                                      .toURI());
        String content = new String(Files.readAllBytes(path), "UTF-8");

        assertNotNull(content);

        return content;

    }

}
