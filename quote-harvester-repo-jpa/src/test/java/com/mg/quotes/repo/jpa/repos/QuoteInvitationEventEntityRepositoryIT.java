package com.mg.quotes.repo.jpa.repos;

import java.time.OffsetDateTime;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mg.quotes.repo.jpa.QuoteHarvesterRepoJpaApplication;
import com.mg.quotes.repo.jpa.entities.QuoteInvitationEventEntity;

/**
 * Integration tests for {@link QuoteInvitationEventEntityRepository}..
 */
@RunWith(SpringRunner.class)
@DataJpaTest
@ContextConfiguration(classes = { QuoteHarvesterRepoJpaApplication.class })
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
// Intentional, otherwise the entire test is executed in a single transaction context, which only gets committed at the
// end of the test, which is undesirable..
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class QuoteInvitationEventEntityRepositoryIT extends BaseRepositoryIT {

    private static final Logger logger = LoggerFactory.getLogger(QuoteInvitationEventEntityRepositoryIT.class);

    @Autowired
    private QuoteInvitationEventEntityRepository qiRepo;

    @Test
    public void storeAndUpdateBasicQuoteInvitationEventEntity_ok() throws RuntimeException {

        logger.info("Invoking storeAndUpdateBasicQuoteInvitationEventEntity_ok()..");

        QuoteInvitationEventEntity qi = createTestQuoteInvitationEventEntity();

        QuoteInvitationEventEntity saved = qiRepo.save(qi);

        logger.info("Saved QuoteInvitationEventEntity={}", saved);

        Assert.assertNotNull(saved);

        Assert.assertTrue(saved.getId() > 0);

        saved.withUpdatedBy("testuser");
        saved.withUpdatedLast(OffsetDateTime.now());

        QuoteInvitationEventEntity updated = qiRepo.save(saved);

        logger.info("Updated QuoteInvitationEventEntity={}", updated);

        Assert.assertEquals("testuser", updated.getUpdatedBy());

        Assert.assertEquals(1L, updated.getVersion().longValue());

    }

    private QuoteInvitationEventEntity createTestQuoteInvitationEventEntity() {

        return new QuoteInvitationEventEntity()
                .withActive(true)
                .withCreatedBy(TEST_USER_ID)
                .withCreatedWhen(OffsetDateTime.now())
                .withUpdatedBy(TEST_USER_ID)
                .withUpdatedLast(OffsetDateTime.now());

    }

}
