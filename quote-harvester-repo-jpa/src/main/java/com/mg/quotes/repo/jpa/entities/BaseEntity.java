package com.mg.quotes.repo.jpa.entities;

import java.time.OffsetDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Base entity for all persisted entities in this module.
 */
@MappedSuperclass
public abstract class BaseEntity {

    public static final int INIT_ODD_HASH = 17;
    public static final int MULTIPLIER_ODD_HASH = 37;

    /**
     * Audit created timestamp..
     */
    @Column(name = "CREATEDATE")
    @NotNull
    private OffsetDateTime createdWhen;

    /**
     * Audit updated timestamp..
     */
    @Column(name = "UPDATEDATE")
    @NotNull
    private OffsetDateTime updatedLast;

    /**
     * Audit created user ID..
     */
    @Column(name = "CREATEBY", length = 2048)
    @NotEmpty
    private String createdBy;

    /**
     * Audit updated user ID..
     */
    @Column(name = "UPDATEBY", length = 2048)
    @NotEmpty
    private String updatedBy;

    /**
     * Version field for optimistic locking..
     */
    @NotNull
    @Version
    @Column(name = "VERSION")
    private Long version;

    /**
     * Active flag..
     */
    @Column(name = "ACTIVE")
    @NotNull
    private Boolean active;

    public abstract <T extends BaseEntity> T getThis();

    public Boolean getActive() {
        return active;
    }

    public OffsetDateTime getCreatedWhen() {
        return createdWhen;
    }

    public OffsetDateTime getUpdatedLast() {
        return updatedLast;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public Long getVersion() {
        return version;
    }

    public <T extends BaseEntity> T withCreatedWhen(OffsetDateTime createdWhen) {
        this.createdWhen = createdWhen;
        return getThis();
    }

    public <T extends BaseEntity> T withUpdatedLast(OffsetDateTime updated) {
        this.updatedLast = updated;
        return getThis();
    }

    public <T extends BaseEntity> T withCreatedBy(String createdBy) {
        this.createdBy = createdBy;
        return getThis();
    }

    public <T extends BaseEntity> T withUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
        return getThis();
    }

    public <T extends BaseEntity> T withVersion(Long version) {
        this.version = version;
        return getThis();
    }

    public <T extends BaseEntity> T withActive(Boolean active) {
        this.active = active;
        return getThis();
    }

}
