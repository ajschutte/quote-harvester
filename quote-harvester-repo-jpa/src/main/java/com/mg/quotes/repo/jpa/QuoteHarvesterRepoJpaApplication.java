package com.mg.quotes.repo.jpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteHarvesterRepoJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuoteHarvesterRepoJpaApplication.class, args);
    }

}
