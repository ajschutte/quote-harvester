package com.mg.quotes.repo.jpa.entities;

/**
 * Base value object for all persisted VOs.
 */
public abstract class BaseValueObject {

    public static final int INIT_ODD_HASH = 17;
    public static final int MULTIPLIER_ODD_HASH = 37;

}
