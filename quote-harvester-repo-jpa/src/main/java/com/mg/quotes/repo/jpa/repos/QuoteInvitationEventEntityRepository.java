package com.mg.quotes.repo.jpa.repos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.mg.quotes.repo.jpa.entities.QuoteInvitationEventEntity;

/**
 * Transactional repository for {@link QuoteInvitationEventEntity} instances.
 */
@Transactional(propagation = Propagation.REQUIRED, isolation = Isolation.READ_COMMITTED)
public interface QuoteInvitationEventEntityRepository extends JpaRepository<QuoteInvitationEventEntity, Long> {

}
