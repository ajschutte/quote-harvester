package com.mg.quotes.repo.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.mg.quotes.models.QuoteInvitationEvent;

/**
 * Persistent entity representing a {@link QuoteInvitationEvent}.
 */
@Entity
@Table(name = "QUOTE_H2_INVITE")
public class QuoteInvitationEventEntity extends BaseEntity {

    @Id
    @Column(name = "ID")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "QUOTE_H2_INVITE_SEQ")
    @SequenceGenerator(sequenceName = "QUOTE_H2_INVITE_SEQ", allocationSize = 1, name = "QUOTE_H2_INVITE_SEQ")
    private Long id;

    public Long getId() {
        return id;
    }

    public QuoteInvitationEventEntity withId(Long id) {
        this.id = id;
        return getThis();
    }

    @Override
    public QuoteInvitationEventEntity getThis() {
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .toString();
    }

}
