--liquibase formatted sql
--changeset quoteh2:1

CREATE SEQUENCE QUOTE_H2_INVITE_SEQ
  START WITH 1
  INCREMENT BY 1;
