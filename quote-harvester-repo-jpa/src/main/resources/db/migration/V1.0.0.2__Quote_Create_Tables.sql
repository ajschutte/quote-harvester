--liquibase formatted sql
--changeset quoteh2:2

CREATE TABLE QUOTE_H2_INVITE
(
  ID                       NUMBER(19, 0)      NOT NULL,
  ACTIVE                   NUMBER(1, 0)       NOT NULL,
  CREATEBY                 VARCHAR2(255 CHAR) NOT NULL,
  CREATEDATE               TIMESTAMP          NOT NULL,
  UPDATEBY                 VARCHAR2(255 CHAR) NOT NULL,
  UPDATEDATE               TIMESTAMP          NOT NULL,
  VERSION                  NUMBER(19, 0)      NOT NULL,
  CONSTRAINT PK_QUOTE_H2_INVITE PRIMARY KEY (ID)
);
