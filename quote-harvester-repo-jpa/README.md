# README - Running Oracle Enterprise 12.2.0.1 as a Docker Container..

Login to the Oracle Container Registry if running for first time (need to register on there first to get a username ans password):

_docker login container-registry.oracle.com_

Will be prompted for the username and password of the registered user..

Then, to start the Oracle Enterprise Container:

_docker run -d -it --name quote-h2 -p 1521:1521 -p 5500:5500 container-registry.oracle.com/database/enterprise:12.2.0.1_

Here, _quote-h2_ is the container's name. 

The first time will take **very** long, since it has to download the Oracle Docker image. 

The default password for _sys_ is _Oradoc_db1_

To verify the container is running:

_docker ps_

This should show "healthy" for the container state (it takes a while to start up)..

Then, to run SQL Plus on it:

_docker exec -it quote-h2 bash -c "source /home/oracle/.bashrc; sqlplus /nolog"_

Here, _quote-h2_ is the container's name, specified when it was started.

Then, _connect sys as sysdba_ to start creating users etc. 

The JDBC URL to connect from an app looks like this:

_jdbc:oracle:thin:@localhost:1521:ORCLCDB_

Here, _ORCLCDB_ is the default SID used when the container starts up (can be changed by passing a custom config file). 

To stop the container:

_docker stop quote-h2_

To restart the stopped container:

_docker restart quote-h2_


