package com.mg.quotes.models;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mg.quotes.models.core.BaseValueType;

/**
 * Value type representing pricesheet details..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PricesheetDescriptor extends BaseValueType {

    private String quotePricesheetKey;

    private String quotePricesheetId;

    private String ratePricesheetId;

    private String oldQuotePricesheetId;

    public String getQuotePricesheetKey() {
        return quotePricesheetKey;
    }

    public PricesheetDescriptor withQuotePricesheetKey(String quotePricesheetKey) {
        this.quotePricesheetKey = quotePricesheetKey;
        return getThis();
    }

    public String getQuotePricesheetId() {
        return quotePricesheetId;
    }

    public PricesheetDescriptor withQuotePricesheetId(String quotePricesheetId) {
        this.quotePricesheetId = quotePricesheetId;
        return getThis();
    }

    public String getRatePricesheetId() {
        return ratePricesheetId;
    }

    public PricesheetDescriptor withRatePricesheetId(String ratePricesheetId) {
        this.ratePricesheetId = ratePricesheetId;
        return getThis();
    }

    public String getOldQuotePricesheetId() {
        return oldQuotePricesheetId;
    }

    public PricesheetDescriptor withOldQuotePricesheetId(String oldQuotePricesheetId) {
        this.oldQuotePricesheetId = oldQuotePricesheetId;
        return getThis();
    }

    @Override
    protected PricesheetDescriptor getThis() {
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("quotePricesheetKey", quotePricesheetKey)
                .append("quotePricesheetId", quotePricesheetId)
                .append("ratePricesheetId", ratePricesheetId)
                .append("oldQuotePricesheetId", oldQuotePricesheetId)
                .toString();
    }

}
