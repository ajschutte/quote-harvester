package com.mg.quotes.models.core;

import java.time.OffsetDateTime;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Resource representing a positive or negative acknowledgment to an attempted event delivery to a consumer..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ConsumerAcknowledgment extends BaseValueType {

    /**
     * Enum representing all possible acknowledgments..
     */
    public enum AckResult { ACCEPTED, SENT, REJECTED, FAILED }

    /**
     * Event ID of the corresponding event..
     */
    @NotEmpty
    private String eventId;

    /**
     * Acknowledgment result..
     */
    @NotNull
    private AckResult ackResult;

    /**
     * Timestamp when acknowledgment occurred..
     */
    @NotNull
    @Past
    private OffsetDateTime ackDate;

    /**
     * Acknowledgment details - this should contain error details in case of a negative acknowledgment..
     */
    private String ackDetails;

    /**
     * Transaction ID of corresponding invitation..
     */
    @NotEmpty
    private String transactionId;

    public String getEventId() {
        return eventId;
    }

    public ConsumerAcknowledgment withEventId(String eventId) {
        this.eventId = eventId;
        return getThis();
    }

    public AckResult getAckResult() {
        return ackResult;
    }

    public ConsumerAcknowledgment withAckResult(AckResult ackResult) {
        this.ackResult = ackResult;
        return getThis();
    }

    public OffsetDateTime getAckDate() {
        return ackDate;
    }

    public ConsumerAcknowledgment withAckDate(OffsetDateTime ackDate) {
        this.ackDate = ackDate;
        return getThis();
    }

    public String getAckDetails() {
        return ackDetails;
    }

    public ConsumerAcknowledgment withAckDetails(String ackDetails) {
        this.ackDetails = ackDetails;
        return getThis();
    }

    public String getTransactionId() {
        return transactionId;
    }

    public ConsumerAcknowledgment withTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return getThis();
    }

    @Override
    protected ConsumerAcknowledgment getThis() {
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("eventId", eventId)
                .append("ackResult", ackResult)
                .append("ackDate", ackDate)
                .append("ackDetails", ackDetails)
                .append("transactionId", transactionId)
                .toString();
    }

}
