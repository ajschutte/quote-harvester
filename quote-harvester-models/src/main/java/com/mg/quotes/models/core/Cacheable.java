package com.mg.quotes.models.core;

import java.io.Serializable;

/**
 * Must be implemented by all resources and value types that are supposed to be cached..
 */
public interface Cacheable extends Serializable {

}
