package com.mg.quotes.models.spi.core;

import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * Simple interface that consumers can choose to implement (it's not mandatory, but if they implement their own interface, it
 * has to conform to the method signature below)..
 */
public interface Consumer<T> {

    /**
     * Called when an event of type T is delivered.
     *
     * @param context The context associated with the event - usually the headers, properties, metadata, etc.
     * @param event The event, of type T..
     */
    void onEvent(
            @NotEmpty Map<String, Object> context,
            @Valid @NotNull T event);

}
