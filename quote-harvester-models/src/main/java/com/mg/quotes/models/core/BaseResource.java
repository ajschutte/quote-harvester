package com.mg.quotes.models.core;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Base class for all core resources..it implements {@link Cacheable} since resources are cacheable..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class BaseResource implements Cacheable {

    public static final int INIT_ODD_HASH = 17;
    public static final int MULTIPLIER_ODD_HASH = 37;

    private Boolean active = true;

    private String createdBy = null;

    private DateTime createdWhen = null;

    private String description = null;

    private Long id = null;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Map<String, String> metadata = new HashMap<>();

    private String name = null;

    private String updatedBy = null;

    private DateTime updatedLast = null;

    private Long version = null;

    //Do not use...required for Jackson to serialize
    protected BaseResource() { }

    protected BaseResource(BaseResource.BaseResourceBuilder builder) {

        this.id = builder.id;
        this.name = builder.name;
        this.description = builder.description;
        this.createdWhen = builder.createdWhen;
        this.createdBy = builder.createdBy;
        this.updatedLast = builder.updatedLast;
        this.updatedBy = builder.updatedBy;
        this.version = builder.version;
        this.metadata = builder.metadata;
        this.active = builder.active;

    }

    public abstract static class BaseResourceBuilder<S extends BaseResource, T extends BaseResourceBuilder>  {

        private final Long id;
        private String name;
        private String description;
        private DateTime createdWhen;
        private DateTime updatedLast;
        private String createdBy;
        private String updatedBy;
        private Long version;
        private Boolean active;
        private Map<String, String> metadata = new HashMap<>();

        public BaseResourceBuilder(Long aid) {
            this.id = aid;
        }

        public T withName(String aName) {
            this.name = aName;
            return getThis();
        }

        public T withDescription(String desc) {
            this.description = desc;
            return getThis();
        }

        public T withCreated(DateTime when, String by) {
            this.createdWhen = when;
            this.createdBy = by;
            return getThis();
        }

        public T withUpdated(DateTime when, String by) {
            this.updatedLast = when;
            this.updatedBy = by;
            return getThis();
        }

        public T withVersion(Long aVersion) {
            this.version = aVersion;
            return getThis();
        }

        public T isActive(Boolean act) {
            this.active = act;
            return getThis();
        }

        public T addMetadata(String key, String value) {
            metadata.put(key, value);
            return getThis();
        }

        public abstract T getThis();

        public abstract S build();

    }

    /**
     * Get active
     * @return active
     **/
    public Boolean getActive() {
        return active;
    }

    /**
     * Get createdBy
     * @return createdBy
     **/
    public String getCreatedBy() {
        return createdBy;
    }

    /**
     * Get createdWhen
     * @return createdWhen
     **/
    public DateTime getCreatedWhen() {
        return createdWhen;
    }

    /**
     * Get description
     * @return description
     **/
    public String getDescription() {
        return description;
    }

    /**
     * Get id
     * @return id
     **/
    public Long getId() {
        return id;
    }

    /**
     * Get metadata
     * @return metadata
     **/
    public Map<String, String> getMetadata() {
        return Collections.unmodifiableMap(metadata);
    }

    /**
     * Get name
     * @return name
     **/
    public String getName() {
        return name;
    }

    /**
     * Get updatedBy
     * @return updatedBy
     **/
    public String getUpdatedBy() {
        return updatedBy;
    }

    /**
     * Get updatedLast
     * @return updatedLast
     **/
    public DateTime getUpdatedLast() {
        return updatedLast;
    }

    /**
     * Get version
     * @return version
     **/
    public Long getVersion() {
        return version;
    }

}
