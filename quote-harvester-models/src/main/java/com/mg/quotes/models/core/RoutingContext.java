package com.mg.quotes.models.core;

import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Simple POJO to represent routing contect for a quote event..
 */
public class RoutingContext {

    @NotEmpty
    private String scopeId;

    @NotEmpty
    private String applicationId;

    @NotEmpty
    private String applicationGroupId;

    @NotEmpty
    private String customerId;

    public String getScopeId() {
        return scopeId;
    }

    public RoutingContext withScopeId(String scopeId) {
        this.scopeId = scopeId;
        return this;
    }

    public String getApplicationId() {
        return applicationId;
    }

    public RoutingContext withApplicationId(String applicationId) {
        this.applicationId = applicationId;
        return this;
    }

    public String getApplicationGroupId() {
        return applicationGroupId;
    }

    public RoutingContext withApplicationGroupId(String applicationGroupId) {
        this.applicationGroupId = applicationGroupId;
        return this;
    }

    public String getCustomerId() {
        return customerId;
    }

    public RoutingContext withCustomerId(String customerId) {
        this.customerId = customerId;
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("scopeId", scopeId)
                .append("applicationId", applicationId)
                .append("applicationGroupId", applicationGroupId)
                .append("customerId", customerId)
                .toString();
    }

}
