package com.mg.quotes.models.spi;

import java.util.concurrent.CompletableFuture;

import com.mg.quotes.models.QuoteInvitationEvent;
import com.mg.quotes.models.core.ConsumerAcknowledgment;

/**
 * Represents a quote invitation sender SPI.
 */
public interface QuoteInvitationSender {

    /**
     * Unique ID, identifying the provider.
     * @return String - the providerId
     */
    String getProviderId();

    /**
     * This method implementation should process a {@link QuoteInvitationEvent} delivery asynchronously. This implementation
     * usually delegates asynchronously to the {@link #sendQuoteInvitation(QuoteInvitationEvent)} method.
     *
     * @param invitation - the QuoteInvitationEvent..
     * @return the non-blocking acknowledgment..
     */
    CompletableFuture<ConsumerAcknowledgment> handleQuoteInvitation(QuoteInvitationEvent invitation);

    /**
     * This method implementation should process a {@link QuoteInvitationEvent} delivery synchronously.
     *
     * @param invitation - the QuoteInvitationEvent..
     * @return the acknowledgment..
     */
    ConsumerAcknowledgment sendQuoteInvitation(QuoteInvitationEvent invitation);

    /**
     * This method implementation should perform any fallback or recovery actions if a {@link QuoteInvitationEvent} is rejected, or if
     * any other permanent failure occurs during the processing. Implemented senders are usually invoked with a retry policy -
     * this method is automatically invoked when that retry policy is exhausted.
     *
     * @param cause - the Throwable that caused the failure..
     * @param invitation - the QuoteInvitationEvent..
     * @return the non-blocking acknowledgment..
     */
    CompletableFuture<ConsumerAcknowledgment> recoverFailedQuoteInvitation(
            Throwable cause,
            QuoteInvitationEvent invitation);

}
