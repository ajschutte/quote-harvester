package com.mg.quotes.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mg.quotes.models.core.BaseValueType;

/**
 * Value type representing a collection of {@link QuoteInvitationEvent} instances belonging to the same transport.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransportQuoteInvitationEvents extends BaseValueType {

    @NotEmpty
    private String transportId;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private List<QuoteInvitationEvent> quoteInvitations = new ArrayList<>();

    public List<QuoteInvitationEvent> getTransportQuoteInvitationEvents() {
        return Collections.unmodifiableList(quoteInvitations);
    }

    public TransportQuoteInvitationEvents addTransportQuoteInvitationEvents(List<QuoteInvitationEvent> events) {
        this.quoteInvitations.addAll(events);
        return this;
    }

    public TransportQuoteInvitationEvents addTransportQuoteInvitationEvents(QuoteInvitationEvent... events) {
        return addTransportQuoteInvitationEvents(Arrays.asList(events));
    }

    public String getTransportId() {
        return transportId;
    }

    public List<QuoteInvitationEvent> getQuoteInvitations() {
        return Collections.unmodifiableList(quoteInvitations);
    }

    @Override
    protected TransportQuoteInvitationEvents getThis() {
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("transportId", transportId)
                .append("quoteInvitations", quoteInvitations)
                .toString();
    }

}
