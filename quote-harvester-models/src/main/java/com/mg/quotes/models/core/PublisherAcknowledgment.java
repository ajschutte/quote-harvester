package com.mg.quotes.models.core;

import java.time.OffsetDateTime;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * This represents a positive or negative acknowledgment of a {@link Publishable} event publish action.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class PublisherAcknowledgment extends BaseValueType {

    /**
     * Enum representing the possible acknowledgment results..
     * FAILED can be interpreted as a temporary failure, the client can safely retry.
     * REJECTED is a permanent failure, the client must not retry.
     * SKIPPED means the current configuration is such that the event is not to be published.
     */
    public enum AckResult { PUBLISHED, REJECTED, SKIPPED, FAILED}

    @Override
    protected PublisherAcknowledgment getThis() {
        return this;
    }

    /**
     * The acknowledgment result..
     */
    @NotNull
    private AckResult ackResult;

    /**
     * Timestamp when the acknowledgment occurred..
     */
    @NotNull
    @Past
    private OffsetDateTime ackDate;

    /**
     * Additional details related to the ack - if an exception caused the ack, this will contain the exception message..
     */
    private String ackDetails;

    /**
     * The transaction ID of the corresponding event..
     */
    @NotEmpty
    private String transactionId;

    /**
     * The event ID of the corresponding event..
     */
    @NotEmpty
    private String eventId;

    public AckResult getAckResult() {
        return ackResult;
    }

    public PublisherAcknowledgment withAckResult(AckResult ackResult) {
        this.ackResult = ackResult;
        return getThis();
    }

    public OffsetDateTime getAckDate() {
        return ackDate;
    }

    public PublisherAcknowledgment withAckDate(OffsetDateTime ackDate) {
        this.ackDate = ackDate;
        return getThis();
    }

    public String getAckDetails() {
        return ackDetails;
    }

    public PublisherAcknowledgment withAckDetails(String ackDetails) {
        this.ackDetails = ackDetails;
        return getThis();
    }

    public String getTransactionId() {
        return transactionId;
    }

    public PublisherAcknowledgment withTransactionId(String transactionId) {
        this.transactionId = transactionId;
        return getThis();
    }

    public String getEventId() {
        return eventId;
    }

    public PublisherAcknowledgment withEventId(String eventId) {
        this.eventId = eventId;
        return getThis();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("ackResult", ackResult)
                .append("ackDate", ackDate)
                .append("ackDetails", ackDetails)
                .append("transactionId", transactionId)
                .append("eventId", eventId)
                .toString();
    }

}
