package com.mg.quotes.models.core;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Base class for all value types..it implements {@link Cacheable} since value types are cacheable..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public abstract class BaseValueType implements Cacheable {

    public static final int INIT_ODD_HASH = 17;
    public static final int MULTIPLIER_ODD_HASH = 37;

    /**
     * This allows subtypes to use fluent API patterns when building..subtypes should use {@code getThis()} instead of {@code this}..
     *
     * @param <T> the subtype..
     * @return instance of subclass..implementations should just return <code>this</code>..
     */
    protected abstract <T extends BaseValueType> T getThis();

}
