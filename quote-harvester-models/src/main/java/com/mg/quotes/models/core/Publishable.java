package com.mg.quotes.models.core;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Interface to be implemented by clients wishing to publish any POJO to the message bus..
 */
public interface Publishable<T> {

    /**
     * This is included for documentation purposes only. It represents the exact routing key template that the message bus is
     * expecting when publishing an event.
     */
    static final String ROUTING_KEY_TEMPLATE = "{scopeId}.{applicationId}.{applicationGroupId}.{customerId}." +
            "{resourceType}.{eventType}";

    static final String ROUTING_KEY_FORMAT = "%s.%s.%s.%s.%s.%s";

    /**
     * This is the event being published..
     *
     * @return The Event..
     */
    @NotNull
    @Valid
    T getEvent();

    /**
     * The {@link RoutingContext} associated with the event sender..
     *
     * @return the RoutingContext..
     */
    @NotNull
    @Valid
    RoutingContext getRoutingContext();

    /**
     * A transaction ID assigned by the event sender..
     *
     * @return the Transaction ID..
     */
    @NotEmpty String getTransactionId();

    /**
     * A globally unique event ID assigned by the event sender..
     *
     * @return the Event ID..
     */
    @NotEmpty String getEventId();

    /**
     * The ResourceType associated with the event. The default is the simple class name of the event..
     *
     * @return the ResourceType..
     */
    @NotEmpty
    default String getResourceType() {
        return getEvent().getClass().getSimpleName();
    }

    /**
     * The EventType associated with the event. The default is the simple class name of the event..
     *
     * @return the EventType..
     */
    @NotEmpty
    default String getEventType() {
        return getEvent().getClass().getSimpleName();
    }

    /**
     * The routing key to use for publishing. The default generates the routing key from the {@link RoutingContext}, this allows the
     * default to be overridden..
     *
     * @return the RoutingKey..
     */
    default String getRoutingKey() {
        return generateRoutingKeyWith(getRoutingContext(), getResourceType(), getEventType());
    }

    static String generateRoutingKeyWith(
            RoutingContext context,
            String resourceType,
            String eventType) {

        return String.format(ROUTING_KEY_FORMAT, context.getScopeId(), context.getApplicationId(), context.getApplicationGroupId(),
                             context.getCustomerId(), resourceType, eventType);

    }

    /**
     * Standard, trivial implementation of {@link Publishable}..
     *
     * @param <T> the Event..
     */
    class StandardPublishable<T> implements Publishable<T> {

        private T event;

        private RoutingContext routingContext;

        private String eventId;

        private String transactionId;

        public StandardPublishable(T event, RoutingContext routingContext, String eventId, String transactionId) {
            this.event = event;
            this.routingContext = routingContext;
            this.eventId = eventId;
            this.transactionId = transactionId;
        }

        @NotNull
        @Override
        public T getEvent() {
            return this.event;
        }

        @Override
        public @NotNull @Valid RoutingContext getRoutingContext() {
            return this.routingContext;
        }

        @Override
        public @NotEmpty String getTransactionId() {
            return this.transactionId;
        }

        @Override
        public @NotEmpty String getEventId() {
            return this.eventId;
        }

        @Override
        public String toString() {
            return new ToStringBuilder(this)
                    .append("event", event)
                    .append("routingContext", routingContext)
                    .append("eventId", eventId)
                    .append("transactionId", transactionId)
                    .toString();
        }

    }

}
