package com.mg.quotes.models;

import java.time.OffsetDateTime;

import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mg.quotes.models.core.BaseValueType;

/**
 * Value type representing a carrier and miscellaneous associated details..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CarrierDescriptor extends BaseValueType {

    @NotEmpty
    private String carrierId;

    @NotEmpty
    private String ownerId;

    private String mode;

    private Boolean allowAlternateSchedule;

    private Boolean forceContractRate;

    private Boolean allowDeclineReason;

    private Boolean declineReasonRequired;

    private Boolean useLocalContact;

    private OffsetDateTime altPickupEarly;

    private OffsetDateTime altPickupLate;

    private OffsetDateTime altDeliverEarly;

    private OffsetDateTime  altDeliverLate;

    private String altEquipment;

    public String getCarrierId() {
        return carrierId;
    }

    public CarrierDescriptor withCarrierId(String carrierId) {
        this.carrierId = carrierId;
        return getThis();
    }

    public String getOwnerId() {
        return ownerId;
    }

    public CarrierDescriptor withOwnerId(String ownerId) {
        this.ownerId = ownerId;
        return getThis();
    }

    public String getMode() {
        return mode;
    }

    public CarrierDescriptor withMode(String mode) {
        this.mode = mode;
        return getThis();
    }

    public Boolean getAllowAlternateSchedule() {
        return allowAlternateSchedule;
    }

    public CarrierDescriptor withAllowAlternateSchedule(Boolean allowAlternateSchedule) {
        this.allowAlternateSchedule = allowAlternateSchedule;
        return getThis();
    }

    public Boolean getForceContractRate() {
        return forceContractRate;
    }

    public CarrierDescriptor withForceContractRate(Boolean forceContractRate) {
        this.forceContractRate = forceContractRate;
        return getThis();
    }

    public Boolean getAllowDeclineReason() {
        return allowDeclineReason;
    }

    public CarrierDescriptor withAllowDeclineReason(Boolean allowDeclineReason) {
        this.allowDeclineReason = allowDeclineReason;
        return getThis();
    }

    public Boolean getDeclineReasonRequired() {
        return declineReasonRequired;
    }

    public CarrierDescriptor withDeclineReasonRequired(Boolean declineReasonRequired) {
        this.declineReasonRequired = declineReasonRequired;
        return getThis();
    }

    public Boolean getUseLocalContact() {
        return useLocalContact;
    }

    public CarrierDescriptor withUseLocalContact(Boolean useLocalContact) {
        this.useLocalContact = useLocalContact;
        return getThis();
    }

    public OffsetDateTime getAltPickupEarly() {
        return altPickupEarly;
    }

    public CarrierDescriptor withAltPickupEarly(OffsetDateTime altPickupEarly) {
        this.altPickupEarly = altPickupEarly;
        return getThis();
    }

    public OffsetDateTime getAltPickupLate() {
        return altPickupLate;
    }

    public CarrierDescriptor withAltPickupLate(OffsetDateTime altPickupLate) {
        this.altPickupLate = altPickupLate;
        return getThis();
    }

    public OffsetDateTime getAltDeliverEarly() {
        return altDeliverEarly;
    }

    public CarrierDescriptor withAltDeliverEarly(OffsetDateTime altDeliverEarly) {
        this.altDeliverEarly = altDeliverEarly;
        return getThis();
    }

    public OffsetDateTime getAltDeliverLate() {
        return altDeliverLate;
    }

    public CarrierDescriptor withAltDeliverLate(OffsetDateTime altDeliverLate) {
        this.altDeliverLate = altDeliverLate;
        return getThis();
    }

    public String getAltEquipment() {
        return altEquipment;
    }

    public CarrierDescriptor withAltEquipment(String altEquipment) {
        this.altEquipment = altEquipment;
        return getThis();
    }

    @Override
    protected CarrierDescriptor getThis() {
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("carrierId", carrierId)
                .append("ownerId", ownerId)
                .append("mode", mode)
                .append("allowAlternateSchedule", allowAlternateSchedule)
                .append("forceContractRate", forceContractRate)
                .append("allowDeclineReason", allowDeclineReason)
                .append("declineReasonRequired", declineReasonRequired)
                .append("useLocalContact", useLocalContact)
                .append("altPickupEarly", altPickupEarly)
                .append("altPickupLate", altPickupLate)
                .append("altDeliverEarly", altDeliverEarly)
                .append("altDeliverLate", altDeliverLate)
                .append("altEquipment", altEquipment)
                .toString();
    }

}
