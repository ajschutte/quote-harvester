package com.mg.quotes.models.spi.core;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import com.mg.quotes.models.core.Publishable;
import com.mg.quotes.models.core.PublisherAcknowledgment;

/**
 * Core interface for event publisher implementations.
 */
public interface Publisher {

    /**
     * Publishes an event represented by a {@link Publishable} - the implementation should be non-blocking.
     *
     * @param publishable The Publishable containing the event..
     * @return PublisherAcknowledgment Positive or negative acknowlegdment with details..
     */
    CompletableFuture<PublisherAcknowledgment> publish(Publishable publishable);

    /**
     * Publishes a collection of events, each represented by a {@link Publishable} - the implementation should be non-blocking.
     *
     * @param publishables The list of Publishable instances..
     * @return List<PublisherAcknowledgment> Positive or negative acks with details..
     */
    default List<CompletableFuture<PublisherAcknowledgment>> publish(List<Publishable> publishables) {
        return publishables.stream().map(this::publish).collect(Collectors.toList());
    }

    /**
     * Returns whether the passed {@link Publishable} will be skipped, given the current configuration..
     *
     * @param publishable The potential Publishable
     * @return boolean Whether the event will be skipped or not..
     */
    boolean toSkip(Publishable publishable);

}
