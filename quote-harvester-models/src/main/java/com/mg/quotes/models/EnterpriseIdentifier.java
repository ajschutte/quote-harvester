package com.mg.quotes.models;

import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mg.quotes.models.core.BaseValueType;

/**
 * Value type representing an enterprise identifier..
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EnterpriseIdentifier extends BaseValueType {

    /**
     * The enterprise ID..
     */
    @NotEmpty
    private String enterpriseId;

    /**
     * The enterprise name..
     */
    private String enterpriseName;

    /**
     * A REST link to the full enterprise resource, if available..
     */
    private Link enterpriseLink;

    /**
     * The application ID to which the enterprise belongs..
     */
    @NotEmpty
    private String applicationId;

    /**
     * The application group ID to which the enterprise belongs..
     */
    private String applicationGroupId;

    /**
     * The customer ID to which the enterprise belongs..
     */
    @NotEmpty
    private String customerId;

    /**
     * The hierarchy ID of the enterprise.
     */
    @NotEmpty
    private String hierarchyId;

    /**
     * The owning enterprise ID..
     */
    @NotEmpty
    private String owningEnterpriseId;

    public String getEnterpriseId() {
        return enterpriseId;
    }

    public EnterpriseIdentifier withEnterpriseId(String enterpriseId) {
        this.enterpriseId = enterpriseId;
        return getThis();
    }

    public String getEnterpriseName() {
        return enterpriseName;
    }

    public EnterpriseIdentifier withEnterpriseName(String enterpriseName) {
        this.enterpriseName = enterpriseName;
        return getThis();
    }

    public Link getEnterpriseLink() {
        return enterpriseLink;
    }

    public EnterpriseIdentifier withEnterpriseLink(Link enterpriseLink) {
        this.enterpriseLink = enterpriseLink;
        return getThis();
    }

    public String getApplicationId() {
        return applicationId;
    }

    public EnterpriseIdentifier withApplicationId(String applicationId) {
        this.applicationId = applicationId;
        return getThis();
    }

    public String getApplicationGroupId() {
        return applicationGroupId;
    }

    public EnterpriseIdentifier withApplicationGroupId(String applicationGroupId) {
        this.applicationGroupId = applicationGroupId;
        return getThis();
    }

    public String getCustomerId() {
        return customerId;
    }

    public EnterpriseIdentifier withCustomerId(String customerId) {
        this.customerId = customerId;
        return getThis();
    }

    public String getHierarchyId() {
        return hierarchyId;
    }

    public EnterpriseIdentifier withHierarchyId(String hierarchyId) {
        this.hierarchyId = hierarchyId;
        return getThis();
    }

    public String getOwningEnterpriseId() {
        return owningEnterpriseId;
    }

    public EnterpriseIdentifier withOwningEnterpriseId(String owningEnterpriseId) {
        this.owningEnterpriseId = owningEnterpriseId;
        return getThis();
    }

    @Override
    protected EnterpriseIdentifier getThis() {
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("enterpriseId", enterpriseId)
                .append("enterpriseName", enterpriseName)
                .append("enterpriseLink", enterpriseLink)
                .append("applicationId", applicationId)
                .append("applicationGroupId", applicationGroupId)
                .append("customerId", customerId)
                .append("hierarchyId", hierarchyId)
                .append("owningEnterpriseId", owningEnterpriseId)
                .toString();
    }

}
