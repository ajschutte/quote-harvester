package com.mg.quotes.models;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.joda.time.DateTime;
import org.springframework.hateoas.Link;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mg.quotes.models.core.BaseResource;
import com.mg.quotes.models.core.RoutingContext;

/**
 * Resource representing a quote invitation event that has occurred and has been published..
 */
@org.springframework.data.couchbase.core.mapping.Document
@JsonInclude(JsonInclude.Include.NON_NULL)
public class QuoteInvitationEvent extends BaseResource {

    /**
     * Enum representing all possible event states..
     */
    public enum QuoteInvitationEventState {CREATED, PUBLISHED, CONSUMED, RETRYING, REJECTED, FAILED, PROCESSED}

    /**
     * A globally unique publisher-supplied invitation event ID..
     */
    @com.couchbase.client.java.repository.annotation.Id
    @org.springframework.data.annotation.Id
    @NotEmpty
    private String invitationId;

    /**
     * If related invitations were sent, this points to the next invitation..
     */
    @com.couchbase.client.java.repository.annotation.Field
    private String nextInvitationId;

    /**
     * Timestamp when the event occurred..
     */
    @com.couchbase.client.java.repository.annotation.Field
    @NotNull
    @Past
    private DateTime occurredWhen;

    /**
     * Timestamp when the invitation expires..
     */
    @com.couchbase.client.java.repository.annotation.Field
    private DateTime expiresWhen;

    /**
     * The transaction ID is a publisher-supplied ID linking multiple requests, messages, and events with a business transaction..
     */
    @NotEmpty
    private String transactionId;

    /**
     * The current state of the event..
     */
    @com.couchbase.client.java.repository.annotation.Field
    @NotNull
    private QuoteInvitationEvent.QuoteInvitationEventState eventState;

    /**
     * The URL of the source system of the event - used when callbacks are required to get additional data..
     */
    @com.couchbase.client.java.repository.annotation.Field
    private Link sourceSystemUrl;

    /**
     * The originating owning enterprise..
     */
    @com.couchbase.client.java.repository.annotation.Field
    @NotNull
    @Valid
    private EnterpriseIdentifier owningEnterprise;

    /**
     * The transport details required for the quote invitation..
     */
    @com.couchbase.client.java.repository.annotation.Field
    @NotNull
    @Valid
    private TransportDescriptor transport;

    private QuoteInvitationEvent() {
        super();
    }

    private QuoteInvitationEvent(QuoteInvitationEventBuilder builder) {

        super(builder);
        this.eventState = builder.eventState;
        this.expiresWhen = builder.expiresWhen;
        this.invitationId = builder.invitationId;
        this.nextInvitationId = builder.nextInvitationId;
        this.occurredWhen = builder.occurredWhen;
        this.owningEnterprise = builder.owningEnterprise;
        this.sourceSystemUrl = builder.sourceSystemUrl;
        this.transactionId = builder.transactionId;
        this.transport = builder.transport;

    }

    /**
     * Public builder for the {@link QuoteInvitationEvent} class.
     */
    public static class QuoteInvitationEventBuilder extends BaseResourceBuilder<QuoteInvitationEvent, QuoteInvitationEventBuilder> {

        private String invitationId;
        private String nextInvitationId;
        private DateTime occurredWhen;
        private DateTime expiresWhen;
        private String transactionId;
        private QuoteInvitationEventState eventState = QuoteInvitationEventState.CREATED;
        private Link sourceSystemUrl;
        private EnterpriseIdentifier owningEnterprise;
        private TransportDescriptor transport;

        public QuoteInvitationEventBuilder() {
            this(null); //Newly created events don't have an ID yet..
        }

        public QuoteInvitationEventBuilder(Long id) {
            super(id);
        }

        public QuoteInvitationEventBuilder withInvitationId(String invitationId) {
            this.invitationId = invitationId;
            return this;
        }

        public QuoteInvitationEventBuilder withNextInvitationId(String nextInvitationId) {
            this.nextInvitationId = nextInvitationId;
            return this;
        }

        public QuoteInvitationEventBuilder withOccurredWhen(DateTime occurredWhen) {
            this.occurredWhen = occurredWhen;
            return this;
        }

        public QuoteInvitationEventBuilder withExpiresWhen(DateTime expiresWhen) {
            this.expiresWhen = expiresWhen;
            return this;
        }

        public QuoteInvitationEventBuilder withTransactionId(String transactionId) {
            this.transactionId = transactionId;
            return this;
        }

        public QuoteInvitationEventBuilder withEventState(QuoteInvitationEventState eventState) {
            this.eventState = eventState;
            return this;
        }

        public QuoteInvitationEventBuilder withSourceSystemUrl(Link sourceSystemUrl) {
            this.sourceSystemUrl = sourceSystemUrl;
            return this;
        }

        public QuoteInvitationEventBuilder withOwningEnterprise(EnterpriseIdentifier owningEnterprise) {
            this.owningEnterprise = owningEnterprise;
            return this;
        }

        public QuoteInvitationEventBuilder withTransport(TransportDescriptor transport) {
            this.transport = transport;
            return this;
        }

        @Override
        public QuoteInvitationEventBuilder getThis() {
            return this;
        }

        @Override
        public QuoteInvitationEvent build() {

            return new QuoteInvitationEvent(this);

        }

    }

    public String getInvitationId() {
        return invitationId;
    }

    public String getNextInvitationId() {
        return nextInvitationId;
    }

    public DateTime getOccurredWhen() {
        return occurredWhen;
    }

    public DateTime getExpiresWhen() {
        return expiresWhen;
    }

    public QuoteInvitationEventState getEventState() {
        return eventState;
    }

    public EnterpriseIdentifier getOwningEnterprise() {
        return owningEnterprise;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public Link getSourceSystemUrl() {
        return sourceSystemUrl;
    }

    public TransportDescriptor getTransport() {
        return transport;
    }

    /**
     * This mutating method is for convenience, it's common to just transition the state, and it's a pain to rebuild the entire event..
     * @param newState - the QuoteInvitationEventState to transition to..
     */
    public void transitionToState(QuoteInvitationEventState newState) {
        this.eventState = newState;
    }

    private void validateContext(RoutingContext context) {

        if ((context == null) || (StringUtils.isAnyBlank(context.getScopeId(), context.getApplicationId(),
                                                         context.getApplicationGroupId(), context.getCustomerId()))) {
            throw new IllegalArgumentException("QuoteInvitationEvent issue - invalid RoutingContext=" + context);
        }

    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        QuoteInvitationEvent that = (QuoteInvitationEvent)o;

        return new EqualsBuilder()
                .append(invitationId, that.invitationId)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(INIT_ODD_HASH, MULTIPLIER_ODD_HASH)
                .append(invitationId)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("invitationId", invitationId)
                .append("nextInvitationId", nextInvitationId)
                .append("occurredWhen", occurredWhen)
                .append("expiresWhen", expiresWhen)
                .append("transactionId", transactionId)
                .append("eventState", eventState)
                .append("sourceSystemUrl", sourceSystemUrl)
                .append("owningEnterprise", owningEnterprise)
                .append("transport", transport)
                .toString();
    }

}
