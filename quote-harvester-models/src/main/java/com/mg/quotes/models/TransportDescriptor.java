package com.mg.quotes.models;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.mg.quotes.models.core.BaseValueType;

/**
 * Value type representing transport details required for a quote.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TransportDescriptor extends BaseValueType {

    @NotEmpty
    private String transportId;

    @Valid
    private CarrierDescriptor carrier;

    @Valid
    private PricesheetDescriptor pricesheet;

    public String getTransportId() {
        return transportId;
    }

    public TransportDescriptor withTransportId(String transportId) {
        this.transportId = transportId;
        return getThis();
    }

    public CarrierDescriptor getCarrier() {
        return carrier;
    }

    public TransportDescriptor withCarrier(CarrierDescriptor carrier) {
        this.carrier = carrier;
        return getThis();
    }

    public PricesheetDescriptor getPricesheet() {
        return pricesheet;
    }

    public TransportDescriptor withPricesheet(PricesheetDescriptor pricesheet) {
        this.pricesheet = pricesheet;
        return getThis();
    }

    @Override
    protected TransportDescriptor getThis() {
        return this;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("transportId", transportId)
                .append("carrier", carrier)
                .append("pricesheet", pricesheet)
                .toString();
    }

}
