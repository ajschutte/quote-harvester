package com.mg.quotes.services;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuoteHarvesterServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(QuoteHarvesterServiceApplication.class, args);
    }

}
